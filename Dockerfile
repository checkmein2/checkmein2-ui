# base on latest ruby base image
FROM node:6.10.0-alpine
# setup app folders
RUN mkdir /app
WORKDIR /app
# copy over Gemfile and install bundle
ADD . /app
RUN yarn
RUN yarn run build
RUN npm prune --production
RUN yarn cache clean
CMD [ "yarn", "start" ]
ENV UI_PORT 9100
EXPOSE 9100