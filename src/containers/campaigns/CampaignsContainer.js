
import { graphql } from 'react-apollo';
import { allCampaignsQuery } from '../../utils/Queries';
import Campaigns from '../../components/campaigns/Campaigns';

export default graphql(allCampaignsQuery)(Campaigns);