import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import * as React from 'react';
import { FileUploader } from '../components/FileUploader';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import PropTypes from 'prop-types';
import styled from 'styled-components';
import { checkA11y } from 'storybook-addon-a11y';
import withCode from 'storybook-addon-code';
import * as Utils from '../../.storybook/utils'

storiesOf('File Uploader', module)
  .addDecorator(checkA11y)
  .add('standard issue', () => (
    Utils.RenderWithTheme(
      <MuiThemeProvider>
        <FileUploader
              buttonLabel={'file uploader'}
              defaultPicture={"https://ksr-ugc.imgix.net/assets/018/017/062/3639b75dd430d036764962d53024a36c_original.png?crop=faces&w=560&h=315&fit=crop&v=1503922815&auto=format&q=92&s=a12a9fd456337c2c1ac71782113ab05f"}
              description={"Add a picture to represent your cause. You`ll raise more money if you do."}
              title={"Picture"} />
      </MuiThemeProvider>
    )
     
  ))