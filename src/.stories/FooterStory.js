import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import * as React from 'react';
import { Footer } from '../components/Footer';

storiesOf('Footer', module)
  .add('standard', () => (
    <Footer />
  ))