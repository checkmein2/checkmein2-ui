import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import * as React from 'react';
import AddCampaign from '../../components/campaigns/AddCampaign';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import * as Utils from '../../../.storybook/utils';
import 'react-quill/dist/quill.snow.css';

storiesOf('Add Campaign', module)
  .add('standard', () => (
    Utils.RenderWithTheme(
      <MuiThemeProvider>
        <AddCampaign />
      </MuiThemeProvider>
    )
  ));