import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import * as React from 'react';
import Campaigns from '../../components/campaigns/Campaigns';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const campaigns = [
  {
    name: 'Test',
    content: "<p>This is a paragraph</p>",
    startDate: 12341,
    endDate: 2134
  }
];
const data = {
  campaigns
};
storiesOf('Campaigns page', module)
  .add('standard', () => (
    <MuiThemeProvider>
      <Campaigns
        data={data}
      />
    </MuiThemeProvider>
  ));