import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { BrowserRouter } from 'react-router-dom';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import {
    ApolloClient,
    createNetworkInterface,
    ApolloProvider
} from 'react-apollo';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
injectTapEventPlugin();

// Create the apollo client
const networkInterface = createNetworkInterface({
    //uri: `${window.location.protocol}//172.18.0.15:8100/graphql`,
    uri: `${window.location.protocol}//${window.location
        .hostname}:8000/graphql`,
    transportBatching: true
});
networkInterface.use([
    {
        applyMiddleware(req, next) {
            if (!req.options.headers) {
                req.options.headers = {}; // Create the header object if needed.
            }
            req.options.headers.authorization = localStorage.getItem('token')
                ? localStorage.getItem('token')
                : '';
            next();
        }
    }
]);

const client = new ApolloClient({
    networkInterface,
    dataIdFromObject: result => {
        if (result.id && result.__typename) {
            return result.__typename + result.id;
        }
        return null;
    }
});

ReactDOM.render(
    <ApolloProvider client={client}>
        <MuiThemeProvider>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </MuiThemeProvider>
    </ApolloProvider>,
    document.getElementById('root')
);
registerServiceWorker();
