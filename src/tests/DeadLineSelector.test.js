import React from 'react';
import { shallow, mount } from 'enzyme';
import { DeadLineSelector } from '../components/DeadLineSelector';
import toJson from 'enzyme-to-json';

describe('DeadLineSelector component', () => {    

    it('renders without crashing', (done) => {
        const wrapper = shallow(<DeadLineSelector />);
        expect(toJson(wrapper)).toMatchSnapshot();
        done();
    });

    it('should call onDeadLineSelect function when it`s clicked', (done) => {        
        const mockCallback = jest.fn();
        const wrapper = shallow(<DeadLineSelector onDeadLineSelect={mockCallback} />);
        expect(mockCallback.mock.calls.length).toBe(0);
        const radioBoxField = wrapper.find('.js-radio-button-2');
        radioBoxField.simulate('click', { event: { target: { value: '2' } } });
        expect(mockCallback.mock.calls.length).toBe(1);
        done(); 
    });

    it('should call onInputClick when it`s clicked', (done) => {
        const mockCallback = jest.fn();
        const wrapper = shallow(<DeadLineSelector onInputClick={mockCallback} />);
        expect(mockCallback.mock.calls.length).toBe(0);
        const datePickerField = wrapper.find('.js-datepicker-button');
        datePickerField.simulate('click');
        expect(mockCallback.mock.calls.length).toBe(1);
        done();
    });

    it('should call onInputClick when it`s clicked', (done) => {
        const mockCallback = jest.fn();
        const wrapper = shallow(<DeadLineSelector onInputClick={mockCallback} />);
        const textBoxField = wrapper.find('.js-textfield-button');
        textBoxField.simulate('click');
        expect(mockCallback.mock.calls.length).toBe(1);
        done();
    })
});