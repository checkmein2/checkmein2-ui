import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import { CampaignCard } from '../components/campaigns/CampaignCard';
import toJson from 'enzyme-to-json';

const getComponent = (props) => {
    const wrapper = shallow(<CampaignCard {...props} />);
    return wrapper;
};

const campaign = {
    startDate: "1501621200000",
    endDate: "1505941200000",
    name: "campaign name",
    content: "<div>campaign content</div>",
    pictureUrl: "https://ksr-ugc.imgix.net/assets/018/017/062/3639b75dd430d036764962d53024a36c_original.png?crop=faces&w=560&h=315&fit=crop&v=1503922815&auto=format&q=92&s=a12a9fd456337c2c1ac71782113ab05f"
}

describe('CampaignCard component', () => {
    it('renders without crashing', (done) => {
        const wrapper = getComponent(campaign);
        expect(toJson(wrapper)).toMatchSnapshot();
        done();
    });
});