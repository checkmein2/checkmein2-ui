import React from 'react';
import { shallow, mount } from 'enzyme';
import { FileUploader } from '../components/FileUploader';
import toJson from 'enzyme-to-json';

const buttonLabel = "Test picture label picture";
const description = "Test description label."
const title = "Image";
const defaultPicture = "https://ksr-ugc.imgix.net/assets/018/017/062/3639b75dd430d036764962d53024a36c_original.png?crop=faces&w=560&h=315&fit=crop&v=1503922815&auto=format&q=92&s=a12a9fd456337c2c1ac71782113ab05f";

describe('FileUploaderSelector component', () => {
    it('renders without crashing', (done) => {
        const wrapper = shallow(<FileUploader 
                                    buttonLabel={buttonLabel}
                                    defaultPicture={defaultPicture}
                                    description={description}
                                    title={title} />);
        expect(toJson(wrapper)).toMatchSnapshot();
        done();
    });
});