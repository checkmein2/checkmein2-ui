const colors = {
    mainBlue:  '#00BCD4',    
    darkGrey: '#626369',
    lightGrey: '#D3D3D3',
    white:  '#ffffff'
}
  
export default colors;