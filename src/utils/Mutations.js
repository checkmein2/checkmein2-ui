import gql from 'graphql-tag';

export const adminLoginMutation = gql`
    mutation adminLogin($username: String!, $password: String!) {
        adminLogin(username: $username, password: $password) {
          jwt
        }
    }
`;

export const userLoginMutation = gql`
    mutation (
        $access_token: String,
        $email: String,
        $name: String,
        $id: String
    ) {
        userLogin(
            profile: {
                access_token: $access_token,
                email: $email,
                name: $name,
                id: $id
            }
        ) {
          jwt
        }
    }
`;

export const createCampaignMutation = gql`
    mutation ($name: String!, $content: String!, $user_id: String!, $startDate: Float!, $endDate: Float!,
              $summary: String!, $deadline_type: Int!, $is_launched: Boolean!, $deadline_run_for: Int)  {
        createCampaign(
            campaign: {
                name: $name, 
                content: $content, 
                user_id: $user_id, 
                startDate: $startDate, 
                endDate: $endDate,
                summary: $summary,
                deadline_type: $deadline_type,                
                is_launched: $is_launched,
                deadline_run_for: $deadline_run_for
        }) {
            _id,
            name, 
            content, 
            user_id, 
            startDate, 
            endDate,
            summary,
            deadline_type,
            is_launched,
            deadline_run_for            
        }
    }
`;

export const removeCampaignMutation = gql`
    mutation ($campaignId: ID!) {
        removeCampaign (
            campaign_id: $campaignId,
        ) {
            _id
        }
  }`;

export const createChallengeMutation = gql`
    mutation ($name: String!, $campaignId: String!, $description: String!, $sponsorDescription: String!,
                $photo: Boolean, $numeric: Int, $lat: Float, $lng: Float) {
        createChalenge(
            challenge:{
                campaign_id: $campaignId,
                name: $name,
                description: $description,
                media_id: "mi",
                sponsor: {
                    description: $sponsorDescription
                },
                checkin_types: {
                    photo: $photo,
                    numeric: $numeric,
                    lat: $lat,
                    lng: $lng
                }
            }
        ) {
            _id,
            campaign_id,
            owner_id,
            name,
            description,
            media_id,
            checkin_types {
                photo,
                numeric,
                lat,
                lng
            },
            sponsors {
                user_id
                description
            },
            participants {
                user_id
            }
          }
        }`;


export const editChallengeMutation = gql`
    mutation ($name: String!, $campaignId: String!, $description: String!, $sponsorDescription: String!,
                $photo: Boolean, $numeric: Int, $lat: Float, $lng: Float, $challengeId: ID!) {
        updateChalenge(
            challenge_id: $challengeId,
            challenge:{
                campaign_id: $campaignId,
                name: $name,
                description: $description,
                media_id: "mi",
                sponsor: {
                    description: $sponsorDescription
                },
                checkin_types: {
                    photo: $photo,
                    numeric: $numeric,
                    lat: $lat,
                    lng: $lng
                }
            }
        ) {
            name,
            _id
            }
        }
`;


export const deleteChallengeMutation = gql`
    mutation ($challengeId: ID!) {
        deleteChallenge (
            challenge_id: $challengeId,
        ) {
            _id,
            success
        }
  }`;

export const addParticipantMutation = gql`
    mutation ($challengeId: ID!) {
        addParticipant(
            challenge_id: $challengeId
        ) {
            _id,
            user_id
        }
    }
`;

export const checkInMutation = gql`
    mutation ($challengeId: ID!, $photo: String, $numeric: Int, $lat: Float, $lng: Float) {
        checkIn(
            checkin:{
                challenge_id: $challengeId,
                checkin_data: {
                    photo: $photo,
                    numeric: $numeric,
                    lat: $lat,
                    lng: $lng
                }
            }
        ) {
            _id,
            completed,
            message
          }
        }`;

export const leaveChallengeMutation = gql`
    mutation ($challengeId: ID!) {
        leaveChallenge(challenge_id: $challengeId) {
            _id,
            user_id
        }
    }
`;

export const uploadPhotoMutation = gql`
    mutation ($fileName: String!, $photo: PhotoType!) {
        uploadPhoto (
            media: {
                fileName: $fileName,
                photo: $photo
            }) {
            _id
            }
    }
`;
