export const isLoggedIn = () =>
    localStorage.getItem('token') !== undefined && localStorage.getItem('token') !== null;
