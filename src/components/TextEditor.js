import React from 'react';
import ReactQuill from 'react-quill';
import { modules } from '../utils/EditorSettings';
import styled from 'styled-components';

const Editor = styled(ReactQuill)`
    height: ${props => props.height};
`;

export const TextEditor = ({ placeholder, onChange, height }) => 
    <Editor modules={modules} placeholder={placeholder} onChange={onChange} height={height} />