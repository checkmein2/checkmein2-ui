import React from 'react';
import styled from 'styled-components';
import { Flex, Box } from 'grid-styled';
import { CampaignCard } from './CampaignCard';

const Title = styled.h1`
    text-align: left;
    color: #00181C;
    font-family: ${props => props.theme.fontFamily};
`;

const hardcodedImage = "https://ksr-ugc.imgix.net/assets/018/017/062/3639b75dd430d036764962d53024a36c_original.png?crop=faces&w=560&h=315&fit=crop&v=1503922815&auto=format&q=92&s=a12a9fd456337c2c1ac71782113ab05f";

export default ({ data: { campaigns, refetch } }) =>
    <div>
        <Title>Campaigns</Title>
        <Flex wrap>
            {campaigns &&
                campaigns.map(campaign =>
                    <Box width={[1, 1/2, 1/3]} px={2} py={3}>
                        <CampaignCard {...campaign} pictureUrl={ hardcodedImage } />
                    </Box>
                )}
        </Flex>
    </div>;
    