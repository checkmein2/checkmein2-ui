import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { Flex, Box } from 'grid-styled';
import { DeadLineSelector } from '../DeadLineSelector';
import { FileUploader } from '../FileUploader';
import styled from 'styled-components';
import { TextEditor } from '../TextEditor';
import { deadLineOptions } from '../../utils/Constants';

const FieldContainer = styled.div`
    display: flex;
    text-align: left;
    padding: 20px;
    margin: 10px;
    border: 0.5px solid ${props => props.theme.colors.lightGrey};
`;

const FieldDescription = styled.div`
    width: 40%;
`;

const FieldContent = styled.div`
    display: flex;
    flex-direction: column;
    width: 60%;
    overflow: auto;
`;

const CharactersCount = styled.label`
    text-align: right;
    color: ${props => props.theme.colors.lightGrey};
`;

const RequireMessage = styled.div`
    color: red;
    margin: 20px;
`;

const AddCampaignButton = styled.div`
    padding: 10px;
    float: right;
`;

const TextEditorContent = styled(FieldContent)`
    height: 150px;
`;

const uploadButtonLabel = "Add a personal picture";
const uploadDescription = "Add a picture to represent your cause. You`ll raise more money if you do."
const uploadTitle = "Picture";
const uploadDefaultPicture = "https://ksr-ugc.imgix.net/assets/018/017/062/3639b75dd430d036764962d53024a36c_original.png?crop=faces&w=560&h=315&fit=crop&v=1503922815&auto=format&q=92&s=a12a9fd456337c2c1ac71782113ab05f";
const editorPlaceholder = "Use your story to inform and inspire.";

export default class AddCampaign extends React.Component {
    constructor(props) {
        super(props);
        this.state = { deadlineType: 1, isLaunched: false };
    }
    isFormValid = () => (
        this.state.name &&
        this.state.summary &&
        (this.state.deadlineType === 1 ||
        (this.state.deadlineType === 2 && this.state.deadLineRunFor) ||
        (this.state.deadlineType === 3 && this.state.deadLineDate))
    )
    addCampaign = (event) => { 
        if (this.isFormValid())
        {
            const startDate = new Date(this.state.startDate);
            const endDate = new Date(this.state.endDate);
            const userId = 1111;
            this.props.saveCampaign(
                this.state.name,
                this.state.content,
                userId,
                startDate.getTime(),
                endDate.getTime(),
                this.state.summary,
                this.state.deadlineType,
                this.state.isLaunched,
                this.state.deadLineRunFor
            );
        }
    }
    handleInputChange = (event) => {
        const { name, value, id } = event.target;
        const eventId = parseInt(id, 10);
        if (eventId === deadLineOptions.RUN_FOR) {
            const deadLineRunFor = parseInt(event.target.id, 10);
            const today = new Date();
            let endDate = new Date();
            endDate.setDate(today.getDate() + parseInt(value, 10));

            this.setState({
                deadLineRunFor,
                startDate: today,
                endDate: endDate
            });
        }
        else
            this.setState({ 
                [name]: value 
            });
    }
    handleStartDateChange = (_, startDate) => {
        this.setState({ 
            startDate 
        });
    }
    handleEndDateChange = (_, endDate) => {
        this.setState({ 
            endDate 
        });
    }
    handleDeadLineDateChange = (_, deadLineDate) =>
    {
        this.setState({ 
            deadLineDate, 
            startDate: new Date(), 
            endDate: deadLineDate 
        });
    }
    onDeadLineSelect = (event) => {
        const deadlineType = parseInt(event.target.value, 10);
        this.setState({ 
            deadlineType, 
            startDate: new Date(), 
            endDate: new Date(5000, 10, 10) 
        });
    }
    onInputClick = (event) => {
        const deadlineType = parseInt(event.target.id, 10);
        this.setState({
            deadlineType
        });
    }
    handleEditorChange = (value) => {
        this.setState({ 
            content: value 
        });
    }
    
    render() {
        return (
            <div>
                <h1>Add Campaign</h1>
                <form className="commentForm" onSubmit={this.handleSubmit}>
                    <Flex wrap>
                        <Box width={1} px={2}>
                            <FieldContainer>
                                <FieldDescription>
                                    <div>
                                        <strong>
                                            What`s your cause?
                                        </strong>
                                    </div>
                                    <div>
                                        <label>
                                            Introduce the <strong>who</strong>, 
                                            <strong> what</strong> and <strong>why </strong> 
                                            of your cause in 255 characters or less.
                                        </label>
                                    </div>
                                </FieldDescription>
                                <FieldContent>
                                    <textarea 
                                        name="summary"
                                        onChange={this.handleInputChange}
                                        maxLength="255"
                                        rows="6" />
                                    <CharactersCount>
                                        {`${this.state.summary ? this.state.summary.length : 0}/255`}
                                    </CharactersCount>
                                </FieldContent>                                
                            </FieldContainer>
                        </Box>
                        <Box width={1} px={2}>
                            <FieldContainer>
                                <FieldDescription>
                                    <div>
                                        <strong>Title</strong>
                                    </div>
                                    <div>
                                        <label>
                                            Give your campaign a title that inspires.
                                        </label>
                                    </div>
                                </FieldDescription>
                                <FieldContent>
                                    <TextField
                                        name="name"
                                        floatingLabelText="Campaign name"
                                        onChange={this.handleInputChange}
                                        rows={1} 
                                        maxLength={50} />
                                        <CharactersCount>
                                            {`${this.state.name ? this.state.name.length : 0}/50`}
                                        </CharactersCount>
                                </FieldContent>
                            </FieldContainer>                          
                        </Box>
                        <Box width={1} px={2}>
                            <FileUploader
                                buttonLabel={uploadButtonLabel}
                                defaultPicture={uploadDefaultPicture}
                                description={uploadDescription}
                                title={uploadTitle} />
                        </Box>
                        <Box width={1} px={2}>
                            <FieldContainer>
                                <FieldDescription>
                                    <div>
                                        <strong>
                                            Story (optional)
                                        </strong>
                                    </div>
                                    <div>
                                        <label>
                                            Tell the world all about your case. Include the 
                                            <strong> 5 essentials </strong> 
                                            and you`ll get more support.
                                        </label>
                                    </div>
                                </FieldDescription>
                                <TextEditorContent>
                                    <TextEditor 
                                        placeholder={editorPlaceholder}
                                        onChange={this.handleEditorChange}
                                        height="100px" />                                    
                                </TextEditorContent>
                            </FieldContainer>
                        </Box>
                        <Box width={1} px={2}>
                            <DeadLineSelector 
                                onDeadLineSelect={this.onDeadLineSelect}
                                handleDeadLineDateChange={this.handleDeadLineDateChange}
                                selectedDeadline={ this.state.deadlineType }
                                handleInputChange={ this.handleInputChange }
                                onInputClick={this.onInputClick} />
                        </Box>                                            
                        {this.isFormValid() ||
                            <Box width={1} px={2}>
                                <RequireMessage>
                                    Please provide all required fields!
                                </RequireMessage>
                            </Box>}
                        <Box width={1} px={2}>
                            <AddCampaignButton>
                                <RaisedButton
                                    primary={true}
                                    label="Add Campaign"
                                    onTouchTap={this.addCampaign}/>
                            </AddCampaignButton>
                        </Box>
                    </Flex>
                </form>
            </div>
        );
    }
}
