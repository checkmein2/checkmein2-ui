import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { login } from '../../services/AuthService';

import { userLoginMutation } from '../../utils/Mutations.js';
import { graphql } from 'react-apollo';

const FacebookLoginButton = ({ mutate }) =>
    <RaisedButton
        label="Facebook Login"
        labelPosition="before"
        onClick={() => login('facebook', mutate)}
        primary={true} />;

export default graphql(userLoginMutation)(FacebookLoginButton);
