import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import { Link } from 'react-router-dom';
import { isAuthenticated, logout } from '../services/AuthService';


class Menu extends React.Component {
    state = {
        valueSingle: '1'
    };

    handleChangeSingle = (event, value) => {
        this.setState({
            valueSingle: value
        });
    };

    handleOpenMenu = () => {
        this.setState({
            openMenu: true
        });
    };
    render() {
        return (
            <div>
                <IconMenu
                    iconButtonElement={
                        <IconButton>
                            <MenuIcon />
                        </IconButton>
                    }
                    onChange={this.handleChangeSingle}
                    value={this.state.valueSingle}>
                    {!isAuthenticated() &&
                        <MenuItem
                            value="1"
                            primaryText="Login"
                            containerElement={<Link to="/campaigns" />}/>}

                    <MenuItem
                        value="2"
                        primaryText="Campaigns"
                        containerElement={<Link to="/campaigns" />}/>

                    <MenuItem
                        value="3"
                        primaryText="Add Campaign"
                        containerElement={<Link to="/add-campaigns" />}/>

                    {isAuthenticated() &&
                        <MenuItem
                            value="4"
                            primaryText="Sign Out"
                            onClick={logout}/>}
                </IconMenu>
            </div>
        );
    }
}

export default () =>
    <AppBar title="CheckMeIn2" iconElementLeft={<Menu />} />
  ;
