import hello from 'hellojs/dist/hello.all';

export const login = (network, mutate) => {
    hello.init({
        facebook: '1274048442643371',
        google:
            '157223853347-g4ohlbei8d4hfr481sc8fshhnlb1mkr5.apps.googleusercontent.com',
        twitter: 'BCQdIWkyxpbxbvNxqDTT8omJE'
    });
    hello(network).login({ scope: 'email' }).then(
        () => {
            const provider = hello(network).getAuthResponse();
            hello(network).api('me').then(me => {
                mutate({
                    variables: {
                        access_token: provider.access_token,
                        email: me.email,
                        name: `${me.first_name} ${me.last_name}` || me.name,
                        id: me.id
                    }
                }).then(resp => {
                    localStorage.setItem('token', resp.data.userLogin.jwt);
                    document.location = '/';
                });
            });
        },
        e => {
            console.log('Signin error: ' + e.error.message);
        }
    );
};

export const logout = function logout(network = 'facebook') {
    hello(network).logout();
    localStorage.removeItem('token');
    // ApiClient.logout();
    document.location = '/';
};

export const isAuthenticated = () => localStorage.getItem('token') !== null;
